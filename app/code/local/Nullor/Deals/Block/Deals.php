<?php

class Nullor_Deals_Block_Deals extends Mage_Catalog_Block_Product_List
{


    public function getLoadedProductCollection()
    {
        if( Mage::getStoreConfig('promo/nullor_deals/deals_category') ){
            $category_id = Mage::getStoreConfig('promo/nullor_deals/deals_category');
        }
        else{
            $category_id = 176;
        }
        $collection = Mage::getModel('catalog/category')->load($category_id)->getProductCollection();
        $product_collection = array();
        foreach( $collection as $product )
        {
            $product_collection[] = Mage::getModel('catalog/product')->load($product->getId());
        }
        return $product_collection;
    }

	protected function getCategoryId()
    {
        if( Mage::getStoreConfig('promo/nullor_deals/deals_category') ){
            return Mage::getStoreConfig('promo/nullor_deals/deals_category');
        }
        else{
            return 176;
        }
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            if (Mage::registry('product')) {
                /** @var Mage_Catalog_Model_Resource_Category_Collection $categories */
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                if ($categories->count()) {
                    $this->setCategoryId($categories->getFirstItem()->getId());
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                    $this->addModelTags($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }

        return $this->_productCollection;
    }
}