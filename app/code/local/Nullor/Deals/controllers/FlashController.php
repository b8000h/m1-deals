<?php
class Nullor_Deals_FlashController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        /* id version */
        /*
        if( Mage::getStoreConfig('promo/nullor_deals/flash') ){
            $product_id = Mage::getStoreConfig('promo/nullor_deals/flash');
        }
        else{
            $product_id = 180;
        }
        $product = Mage::getModel('catalog/product')->load($product_id);
        */

        /* sku version */
        if( Mage::getStoreConfig('promo/nullor_deals/flash') ){
            $product_sku = Mage::getStoreConfig('promo/nullor_deals/flash');
        }
        else{
            $product_sku = 'X1069';
        }

        $product = Mage::getModel('catalog/product');
        $product->load($product->getIdBySku($product_sku));

    	Mage::register('product', $product);
		Mage::register('current_product', $product);

    	//echo "It works";
        $this->loadLayout();
        $this->renderLayout();
    }

    public function commingAction()
    {
        /* id version */
        /*
        if( Mage::getStoreConfig('promo/nullor_deals/flash_comming') ){
            $product_id = Mage::getStoreConfig('promo/nullor_deals/flash_comming');
        }
        else{
            $product_id = 144;
        }
        $product = Mage::getModel('catalog/product')->load($product_id);
        */

        /* sku version */
        if( Mage::getStoreConfig('promo/nullor_deals/flash_comming') ){
            $product_sku = Mage::getStoreConfig('promo/nullor_deals/flash_comming');
        }
        else{
            $product_sku = 'X1069';
        }

        $product = Mage::getModel('catalog/product');
        $product->load($product->getIdBySku($product_sku));

    	Mage::register('product', $product);
    	Mage::register('current_product', $product);

    	//echo "It works";
        $this->loadLayout();
        $this->renderLayout();
    }
}