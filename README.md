# README #

TODO: 
如何根据 name 来获得 category，根据 sku 来获得 product
查明为什么 productCollection 中得到的 product 没有 attribute extra




### 1 ###

最初的做法是配合 Module 的 controller 建一个 update handler，仿 catalog_product_view
出现的问题:
1. 请求的是 /deals/css/styles.css，显然此处是没有文件的。
因为相对路径，须得为当前 theme 定制
重点抓 head 部分 theme 与 module 的区别
2. tab (description review attributes)不出现。


### 2 ###
考虑只为一个页面施用一个 theme


### 3 ###
因为太多的 update handler 通过 reference handle='catalog_product_view' 为其添砖加瓦
强行为 Module 建一个 update handler 来仿原 catalog_product_view，势必要求所有 reference 到 catalog_product_view 的 update handler 都同样为新的 update handler 做同样的操作，影响太大

产品的选择在后台开辟个设置项页即可

通过 url 来判断可确保 deals 页 和 view 页同时存在

TODO: 在 layout 中判断，更换 phtml
需要在 catalog/product/view.phtml 首添加 router
```
<?php if( !strpos( Mage::helper('core/url')->getCurrentUrl(), 'deals/cybermonday/comming') === false ): ?>
<?php //echo Mage::helper('core/url')->getCurrentUrl(); ?>
<?php require dirname(__FILE__).'/../../deals/cybermonday_comming.phtml' ?>

<?php elseif( !strpos( Mage::helper('core/url')->getCurrentUrl(), 'deals/cybermonday') === false ): ?>
<?php require dirname(__FILE__).'/../../deals/cybermonday.phtml' ?>

<?php elseif( !strpos( Mage::helper('core/url')->getCurrentUrl(), 'deals/flash/comming') === false ): ?>
<?php //echo Mage::helper('core/url')->getCurrentUrl(); ?>
<?php require dirname(__FILE__).'/../../deals/flash_comming.phtml' ?>

<?php elseif( !strpos( Mage::helper('core/url')->getCurrentUrl(), 'deals/flash') === false ): ?>
<?php require dirname(__FILE__).'/../../deals/flash.phtml' ?>

<?php else: ?>

<?php // 此处放 view.phtml 原内容 ?>

<?php endif; ?>
```

PHP Interpretor 所理解的相对路径是基于 current work dir，而非 current file
require 的部分，直接 require '../../deals/cybermonday.phtml' 是会报错的
使用 dirname(__FILE__) 来解决
因为 dirname(__FIEL__) 末尾不带 /，得自己来